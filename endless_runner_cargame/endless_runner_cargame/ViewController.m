//
//  ViewController.m
//  endless_runner_cargame
//
//  Created by Vikram on 3/15/17.
//  Copyright © 2017 endless_runner. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIView *GameView;
@property (weak, nonatomic) IBOutlet UIView *gameview1;
@property (weak, nonatomic) IBOutlet UIView *gameview2;
@property (weak, nonatomic) IBOutlet UIImageView *leftcar;
@property (weak, nonatomic) IBOutlet UIImageView *rightcar;
@property (weak, nonatomic) IBOutlet UIView *StartScreen;
@property (weak, nonatomic) IBOutlet UIImageView *StartScreenImage;
@property (weak, nonatomic) IBOutlet UIView *GameOverScreen;
@property (weak, nonatomic) IBOutlet UILabel *CurrentScore;
@property (weak, nonatomic) IBOutlet UILabel *CurrentHighScore;

@end

@implementation ViewController
@synthesize bricks;
@synthesize obstaclecarsleft,obstaclecarsright;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _StartScreenImage.image = [UIImage imageNamed:@"GameIconImage.jpg"];
    
    colisions=0;
    gameovercount=0;
    
    startscreentimer=[NSTimer scheduledTimerWithTimeInterval:7 target:self selector:@selector(startscreentimercode) userInfo:nil repeats:NO];
    _StartScreen.hidden=NO;
    _gameview1.hidden=YES;
    _gameview2.hidden=YES;
    _GameOverScreen.hidden=YES;
    
    highscore=[[[NSUserDefaults standardUserDefaults] objectForKey:@"HighScore"] intValue ];
    
    score=0;
    
    _CurrentScore.text=[NSString stringWithFormat:@"%d",score];
    _CurrentHighScore.text=[NSString stringWithFormat:@"%d",highscore];
    
    NSString *path = [[NSBundle mainBundle]
                      pathForResource:@"GameStartSound" ofType:@"mp3"];
    audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                   [NSURL fileURLWithPath:path] error:NULL];
    [audioPlayer play];
}

-(void)startscreentimercode
{
    roadtranstimer=[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(roadtransitioncode) userInfo:nil repeats:YES];
    
    obstacletranstimer=[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(obstacletransitioncode) userInfo:nil repeats:YES];
    
    CGRect bounds = [_gameview1 bounds];
    float leftcarx=bounds.size.width/4;
    float leftcary=CGRectGetMaxY(_gameview1.frame)-60;
    
    _leftcar.frame = CGRectMake(
                                0,
                                0, bounds.size.width/4, bounds.size.height/7);
    [_leftcar setCenter:CGPointMake(leftcarx,leftcary)];
    
    bounds = [_gameview2 bounds];
    float rightcarx=bounds.size.width/4;
    float rightcary=CGRectGetMaxY(_gameview1.frame)-60;
    _rightcar.frame = CGRectMake(
                                 0,
                                 0, bounds.size.width/4, bounds.size.height/7);
    [_rightcar setCenter:CGPointMake(rightcarx,rightcary)];
    
    obstaclecount=0;
    
    bricks = [[NSMutableArray alloc] init];
    obstaclecarsleft = [[NSMutableArray alloc] init];
    obstaclecarsright = [[NSMutableArray alloc] init];
    
    ObstacleCar *demomyImageObj;
    
    imagePath = [UIImage imageNamed:@"Obstacle2.jpg"];
    //myImageObj = [[UIImageView alloc] initWithImage:imagePath];
    demomyImageObj = [[ObstacleCar alloc] initWithImage:imagePath];
    
    for (obstaclecount=0; obstaclecount<7; obstaclecount++) {
        
        demomyImageObj.frame = CGRectMake(
                                      0,
                                      0, bounds.size.width/4, bounds.size.height/7);
        
        
        
        
       [demomyImageObj setCenter:CGPointMake(0,0)];
        [obstaclecarsleft addObject:demomyImageObj];
         [obstaclecarsright addObject:demomyImageObj];
        
         // NSLog(@"Object Added at Index %d...",obstaclecount);
    }
    obstaclecount=0;
    
    [self makeBricks:nil];
    _StartScreen.hidden=YES;
    _gameview1.hidden=NO;
    _gameview2.hidden=NO;
}

-(void)obstacletransitioncode
{
    obstaclecount=obstaclecount%6;
    //NSLog(@"Obstacle Count:%d",obstaclecount);
    int random=rand();
    int position,obstaclenum;
    position=random%2;
    obstaclenum=random%4;
   // NSLog(@"Randon:%d",random);
   // NSLog(@"Position:%d",position);
   // NSLog(@"ObstacleNum:%d",obstaclenum);
    
    ObstacleCar *myImageObj;
    
    switch (obstaclenum)
    {
        case 0:
            imagePath = [UIImage imageNamed:@"Obstacle1.jpg"];
            //myImageObj = [[UIImageView alloc] initWithImage:imagePath];
            myImageObj= [[ObstacleCar alloc] initWithImage:imagePath];
            break;
            
        case 1:
            imagePath = [UIImage imageNamed:@"Obstacle2.jpg"];
            //myImageObj = [[UIImageView alloc] initWithImage:imagePath];
            myImageObj = [[ObstacleCar alloc] initWithImage:imagePath];
            break;
            
        case 2:
            imagePath = [UIImage imageNamed:@"Obstacle3.jpg"];
            //myImageObj = [[UIImageView alloc] initWithImage:imagePath];
            myImageObj = [[ObstacleCar alloc] initWithImage:imagePath];
            break;
        
        case 3:
            imagePath = [UIImage imageNamed:@"Obstacle4.jpg"];
            //myImageObj = [[UIImageView alloc] initWithImage:imagePath];
            myImageObj = [[ObstacleCar alloc] initWithImage:imagePath];
            break;
        
    }
    
    CGRect bounds = [_gameview1 bounds];
    float leftcarx;
    float leftcary;
    
    
    switch (position)
    {
        case 0:
            leftcarx=bounds.size.width/4;
            leftcary=50;
            
            
            myImageObj.frame = CGRectMake(
                                          0,
                                          0, bounds.size.width/4, bounds.size.height/7);
            
            [_gameview1 addSubview:myImageObj];
            [myImageObj setCenter:CGPointMake(leftcarx,leftcary)];
           // [obstaclecars addObject:myImageObj];
            //[obstaclecars insertObject:myImageObj atIndex:obstaclecount];
            [obstaclecarsleft replaceObjectAtIndex:obstaclecount withObject:myImageObj];
            obstaclecount++;
            break;
            
        case 1:
            leftcarx=(bounds.size.width/4)*3;
            leftcary=50;
            
            
            myImageObj.frame = CGRectMake(
                                          0,
                                          0, bounds.size.width/4, bounds.size.height/7);
            
            [_gameview1 addSubview:myImageObj];
            [myImageObj setCenter:CGPointMake(leftcarx,leftcary)];
            //[obstaclecars addObject:myImageObj];
            //[obstaclecars insertObject:myImageObj atIndex:obstaclecount];
            [obstaclecarsleft replaceObjectAtIndex:obstaclecount withObject:myImageObj];
            obstaclecount++;
            break;
    }
    
    
    int random2=rand();
    int position2,obstaclenum2;
    position2=random2%2;
    obstaclenum2=random2%4;
    // NSLog(@"Randon:%d",random);
    // NSLog(@"Position:%d",position);
    // NSLog(@"ObstacleNum:%d",obstaclenum);
    
    
    switch (obstaclenum2)
    {
        case 0:
            imagePath = [UIImage imageNamed:@"Obstacle1.jpg"];
            //myImageObj = [[UIImageView alloc] initWithImage:imagePath];
            myImageObj= [[ObstacleCar alloc] initWithImage:imagePath];
            break;
            
        case 1:
            imagePath = [UIImage imageNamed:@"Obstacle2.jpg"];
            //myImageObj = [[UIImageView alloc] initWithImage:imagePath];
            myImageObj = [[ObstacleCar alloc] initWithImage:imagePath];
            break;
            
        case 2:
            imagePath = [UIImage imageNamed:@"Obstacle3.jpg"];
            //myImageObj = [[UIImageView alloc] initWithImage:imagePath];
            myImageObj = [[ObstacleCar alloc] initWithImage:imagePath];
            break;
            
        case 3:
            imagePath = [UIImage imageNamed:@"Obstacle4.jpg"];
            //myImageObj = [[UIImageView alloc] initWithImage:imagePath];
            myImageObj = [[ObstacleCar alloc] initWithImage:imagePath];
            break;
            
    }
    
    CGRect bounds2 = [_gameview2 bounds];
    float rightcarx;
    float rightcary;
    
    
    switch (position2)
    {
        case 0:
            rightcarx=bounds.size.width/4;
            rightcary=50;
            
            
            myImageObj.frame = CGRectMake(
                                          0,
                                          0, bounds2.size.width/4, bounds2.size.height/7);
            
            [_gameview2 addSubview:myImageObj];
            [myImageObj setCenter:CGPointMake(rightcarx,rightcary)];
            //[obstaclecars addObject:myImageObj];
            //[obstaclecars insertObject:myImageObj atIndex:obstaclecount];
            [obstaclecarsright replaceObjectAtIndex:obstaclecount withObject:myImageObj];
            obstaclecount++;
            break;
            
        case 1:
            rightcarx=(bounds.size.width/4)*3;
            rightcary=50;
            
            
            myImageObj.frame = CGRectMake(
                                          0,
                                          0, bounds2.size.width/4, bounds2.size.height/7);
            
            [_gameview2 addSubview:myImageObj];
            [myImageObj setCenter:CGPointMake(rightcarx,rightcary)];
            [obstaclecarsright replaceObjectAtIndex:obstaclecount withObject:myImageObj];
            obstaclecount++;
            break;
    }
    

    
}

-(void)roadtransitioncode
{
    float yaxis=0;
    
    for (RoadBrick *brick in bricks)
    {
        [brick setCenter:CGPointMake(brick.center.x, brick.center.y+10)];
        if(brick.center.y > (CGRectGetMaxY(_GameView.frame)))
        {
            [brick setCenter:CGPointMake(brick.center.x,yaxis)];
        }
    }
   
    for(ObstacleCar *image in obstaclecarsleft)
    {
       
        [image setCenter:CGPointMake(image.center.x, image.center.y+4)];
        
        if(CGRectIntersectsRect(_leftcar.frame, image.frame))
        {
            gameovercount++;
           // NSLog(@"Left!");
          
            _StartScreen.hidden=YES;
            _gameview1.hidden=YES;
            _gameview2.hidden=YES;
            _GameOverScreen.hidden=NO;
            
            if(gameovercount==1)
            {
            NSString *path = [[NSBundle mainBundle]
                              pathForResource:@"GameOver" ofType:@"mp3"];
            audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                           [NSURL fileURLWithPath:path] error:NULL];
            [audioPlayer play];
            }
        }
        
      if((image.center.y>_leftcar.center.y) && (image.center.y<(_leftcar.center.y+10)))
      {
          score=score+1;
      }
        
    }
    
    for(ObstacleCar *image in obstaclecarsright)
    {
        [image setCenter:CGPointMake(image.center.x, image.center.y+4)];
        
        if(CGRectIntersectsRect(_rightcar.frame, image.frame))
        {
            gameovercount++;
            //NSLog(@"Right!");
            
           _StartScreen.hidden=YES;
            _gameview1.hidden=YES;
           _gameview2.hidden=YES;
            _GameOverScreen.hidden=NO;
           
            if(gameovercount==1)
            {
            NSString *path = [[NSBundle mainBundle]
                              pathForResource:@"GameOver" ofType:@"mp3"];
            audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                           [NSURL fileURLWithPath:path] error:NULL];
            [audioPlayer play];
            }
            
        }
        
        if((image.center.y>_rightcar.center.y) && (image.center.y<(_rightcar.center.y+10)))
        {
            score=score+1;
        }
        
    }
    
   _CurrentScore.text=[NSString stringWithFormat:@"%d",score];
    
    
    if(score>highscore)
    {
        highscore=score;
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:highscore] forKey:@"HighScore"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        _CurrentHighScore.text=[NSString stringWithFormat:@"%d",highscore];
    }
    
    
    
}


-(IBAction)makeBricks:(id)sender
{
    
    CGRect bounds = [_gameview1 bounds];
    float width = 4;
    float height = 20;
    float xaxis=bounds.size.width/2;
    float yaxis=30;
    
    
    for (int i = 0; i < 10; ++i)
    {
        RoadBrick *b = [[RoadBrick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [b setBackgroundColor:[UIColor whiteColor]];
        [_gameview1 addSubview:b];
        [b setCenter:CGPointMake(xaxis,yaxis)];
        [bricks addObject:b];
        yaxis+=100;
    }
    
    bounds = [_gameview2 bounds];
    xaxis=bounds.size.width/2;
    yaxis=30;
    
    for (int i = 0; i < 10; ++i)
    {
        RoadBrick *b = [[RoadBrick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [b setBackgroundColor:[UIColor whiteColor]];
        [_gameview2 addSubview:b];
        [b setCenter:CGPointMake(xaxis,yaxis)];
        [bricks addObject:b];
        yaxis+=100;
    }
   
    bounds = [_GameView bounds];
    xaxis=(bounds.size.width/4)*2;
    yaxis=30;
    
    for (int i = 0; i < CGRectGetMaxY(_GameView.frame); ++i)
    {
        RoadBrick *b = [[RoadBrick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [b setBackgroundColor:[UIColor whiteColor]];
        [_gameview1 addSubview:b];
        [b setCenter:CGPointMake(xaxis,yaxis)];
        
        yaxis+=10;
    }
    
    
}

- (IBAction)leftcarshift:(UIGestureRecognizer *)sender {
    
    UISwipeGestureRecognizerDirection dir=[(UISwipeGestureRecognizer *)sender direction];
    
    
    CGRect bounds = [_gameview1 bounds];
    float leftcarx;
    float leftcary;
    
    
    
    
    switch (dir) {
        case UISwipeGestureRecognizerDirectionRight:
            leftcarx=(bounds.size.width/4)*3;
            leftcary=CGRectGetMaxY(_gameview1.frame)-60;
            _leftcar.frame = CGRectMake(
                                        _leftcar.frame.origin.x,
                                        _leftcar.frame.origin.y, bounds.size.width/4, bounds.size.height/7);
            [_leftcar setCenter:CGPointMake(leftcarx,leftcary)];
            break;
            
        case UISwipeGestureRecognizerDirectionLeft:
            leftcarx=bounds.size.width/4;
            leftcary=CGRectGetMaxY(_gameview1.frame)-60;
            _leftcar.frame = CGRectMake(
                                        _leftcar.frame.origin.x,
                                        _leftcar.frame.origin.y, bounds.size.width/4, bounds.size.height/7);
            [_leftcar setCenter:CGPointMake(leftcarx,leftcary)];
            
            break;
            
            
        default:
            break;
    }
    
    
    
}

- (IBAction)rightcarshift:(UIGestureRecognizer *)sender {
    
    UISwipeGestureRecognizerDirection dir=[(UISwipeGestureRecognizer *)sender direction];
    
   CGRect bounds = [_gameview2 bounds];
    float rightcarx;
    float rightcary;
    
    switch (dir) {
        case UISwipeGestureRecognizerDirectionRight:
            rightcarx=(bounds.size.width/4)*3;
            rightcary=CGRectGetMaxY(_gameview1.frame)-60;
            _rightcar.frame = CGRectMake(
                                         _rightcar.frame.origin.x,
                                         _rightcar.frame.origin.y, bounds.size.width/4, bounds.size.height/7);
            [_rightcar setCenter:CGPointMake(rightcarx,rightcary)];
            
            break;
            
        case UISwipeGestureRecognizerDirectionLeft:
            rightcarx=bounds.size.width/4;
            rightcary=CGRectGetMaxY(_gameview1.frame)-60;
            _rightcar.frame = CGRectMake(
                                         _rightcar.frame.origin.x,
                                         _rightcar.frame.origin.y, bounds.size.width/4, bounds.size.height/7);
            [_rightcar setCenter:CGPointMake(rightcarx,rightcary)];
                        break;
            
            
        default:
            break;
    }
    
    
    
}
@end
