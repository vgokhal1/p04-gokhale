//
//  main.m
//  endless_runner_cargame
//
//  Created by Vikram on 3/15/17.
//  Copyright © 2017 endless_runner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
