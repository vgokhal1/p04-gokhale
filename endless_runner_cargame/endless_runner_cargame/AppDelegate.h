//
//  AppDelegate.h
//  endless_runner_cargame
//
//  Created by Vikram on 3/15/17.
//  Copyright © 2017 endless_runner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

