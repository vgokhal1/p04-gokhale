//
//  ViewController.h
//  endless_runner_cargame
//
//  Created by Vikram on 3/15/17.
//  Copyright © 2017 endless_runner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoadBrick.h"
#import "ObstacleCar.h"
#import <AVFoundation/AVFoundation.h>


int obstaclecount,colisions,score,highscore,gameovercount;
@interface ViewController : UIViewController
{
    NSTimer *roadtranstimer,*obstacletranstimer,*startscreentimer;
    UIImage *imagePath;
    AVAudioPlayer *audioPlayer;
   // UIImageView *myImageObj;
}
- (IBAction)leftcarshift:(UIGestureRecognizer *)sender;

- (IBAction)rightcarshift:(UIGestureRecognizer *)sender;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic, strong) NSMutableArray *obstaclecarsleft,*obstaclecarsright;



@end

